import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DepartmentsRoutingModule} from './departments-routing.module';
import {ListDepartmentsComponent} from './components/list-departments/list-departments.component';
import {DepartmentsService} from './services/departments.service';
import {CreateDepartmentComponent} from './components/create-department/create-department.component';
import {FormsModule} from '@angular/forms';


@NgModule({
    declarations: [
        ListDepartmentsComponent,
        CreateDepartmentComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        DepartmentsRoutingModule
    ],
    providers: [
        DepartmentsService,
    ]
})
export class DepartmentsModule {
}
