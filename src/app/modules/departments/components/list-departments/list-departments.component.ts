import {Component, OnInit} from '@angular/core';
import {Department} from '../../entities/department';
import {DepartmentsService} from '../../services/departments.service';

@Component({
    selector: 'app-list-departments',
    templateUrl: './list-departments.component.html',
    styleUrls: ['./list-departments.component.css']
})
export class ListDepartmentsComponent implements OnInit {

    departments: Department[] = [];

    constructor(private departmentsService: DepartmentsService) {
    }

    ngOnInit(): void {
        this.departmentsService.getAll().subscribe(result => {
            this.departments = result;
        });
    }
}
