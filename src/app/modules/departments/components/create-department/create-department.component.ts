import {Component, OnInit} from '@angular/core';
import {DepartmentsService} from '../../services/departments.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Department} from '../../entities/department';

@Component({
    selector: 'app-create-department',
    templateUrl: './create-department.component.html',
    styleUrls: ['./create-department.component.css']
})
export class CreateDepartmentComponent implements OnInit {

    id: number = null;
    title: string = null;

    constructor(private departmentService: DepartmentsService,
                private router: Router, private activatedRoute: ActivatedRoute) {
        this.id = this.activatedRoute.snapshot.params.departmentId || null;
    }

    ngOnInit(): void {
    }

    submit() {
        const department: Department = {
            id: this.id,
            title: this.title
        };

        if (this.id === null) {
            this.departmentService.create(department).subscribe(result => {
                alert('Record has been created!');
                this.router.navigateByUrl('/departments');
            });
        } else {
            this.departmentService.update(department).subscribe(result => {
                alert('Record has been update!');
                this.router.navigateByUrl('/departments');
            });
        }
    }
}
