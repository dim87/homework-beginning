import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListDepartmentsComponent} from './components/list-departments/list-departments.component';
import {CreateDepartmentComponent} from './components/create-department/create-department.component';


const routes: Routes = [
    {
        path: 'departments',
        children: [
            {
                path: '',
                component: ListDepartmentsComponent
            },
            {
                path: 'create',
                component: CreateDepartmentComponent
            },
            {
                path: 'update/:departmentId',
                component: CreateDepartmentComponent
            },

        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DepartmentsRoutingModule {
}
