import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Department} from '../entities/department';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class DepartmentsService {

    private readonly apiUrl = 'http://localhost:8080/departments';

    constructor(private httpClient: HttpClient) {
    }

    getAll(): Observable<Department[]> {
        return this.httpClient.get<Department[]>(this.apiUrl);
    }

    create(department: Department): Observable<Department> {
        return this.httpClient.put<Department>(this.apiUrl, department);
    }

    update(department: Department): Observable<Department> {
        return this.httpClient.patch<Department>(this.apiUrl, department);
    }
}
